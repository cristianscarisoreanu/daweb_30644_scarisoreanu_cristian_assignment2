import style from "./style.module.css"
import image from "../../images/98fa5c76-6012-4575-b11e-9b0492bee467about-us.jpg";
import {useEffect, useState} from "react";
import CardGrid from "../../shared/CardGrid/CardGrid";
import axios from "axios";
import Cookies from "js-cookie";
import LoadingSpinner from "../../shared/LoadingSpinner/LoadingSpinner";

const prepareData = (data) => {
  return [
    '',
    `${process.env.REACT_APP_BACKEND_URL}${data.image}`,
    data.description,
    parseInt(data.price) + ' €',
    'Vizualizeaza',
    '32rem'
  ]
}

const Rentals = ({type}) => {
  const [apartments, setApartments] = useState([])
  const [houses, setHouses] = useState([])
  const [spaces, setSpaces] = useState([])
  const [loading, setLoading] = useState(false)


  useEffect(() => {
    const headers = document.querySelectorAll('h2')
    if (type === 'apartments') {
      headers[0].scrollIntoView()
    } else if (type === 'houses') {
      headers[1].scrollIntoView()
    } else {
      headers[2].scrollIntoView()
    }
  })

  useEffect(() => {
    setLoading(true)
    axios.get(`${process.env.REACT_APP_BACKEND_URL}/rentals/apartments`, {
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken'),
        'Cookie': `sessionid=${Cookies.get('sessionid')}`,
      },
      withCredentials: true
    })
        .then((response) => {
          let array = []
          response.data.forEach((data) => {
            array.push(prepareData(data))
          })
          setApartments(array)
        })
        .catch((error) => {
          console.error(error)
        })
        .finally(() => {
          setLoading(false)
        })

    axios.get(`${process.env.REACT_APP_BACKEND_URL}/rentals/houses`, {
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken'),
        'Cookie': `sessionid=${Cookies.get('sessionid')}`,
      },
      withCredentials: true
    })
        .then((response) => {
          let array = []
          response.data.forEach((data) => {
            array.push(prepareData(data))
          })
          setHouses(array)
        })
        .catch((error) => {
          console.error(error)
        })
        .finally(() => {
          setLoading(false)
        })

    axios.get(`${process.env.REACT_APP_BACKEND_URL}/rentals/spaces`, {
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken'),
        'Cookie': `sessionid=${Cookies.get('sessionid')}`,
      },
      withCredentials: true
    })
        .then((response) => {
          let array = []
          response.data.forEach((data) => {
            array.push(prepareData(data))
          })
          setSpaces(array)
        })
        .catch((error) => {
          console.error(error)
        })
        .finally(() => {
          setLoading(false)
        })
  }, [])


  return (
      <div>
        <LoadingSpinner show={loading}/>
        <div className={style.divImage}>
          <img className={style.splitImage} src={image} alt={""}/>
          <h1 style={{color: 'black'}}>Inchirieri</h1>
        </div>
        <CardGrid title={'Apartamente de inchiriat'} cardDetails={apartments} divider applyPadding/>
        <CardGrid title={'Case de inchiriat'} cardDetails={houses} divider applyPadding/>
        <CardGrid title={'Spatii de inchiriat'} cardDetails={spaces} divider applyPadding/>
      </div>
  )
}

export default Rentals