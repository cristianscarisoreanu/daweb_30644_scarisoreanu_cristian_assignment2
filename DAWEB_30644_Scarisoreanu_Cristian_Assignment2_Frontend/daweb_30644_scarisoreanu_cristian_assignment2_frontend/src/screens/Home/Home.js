import AppNavbar from "../../shared/AppNavbar/AppNavbar";
import background from '../../images/background.jfif'
import style from './style.module.css'
import socials from "../../images/PngItem_39404.png";
import LoadingSpinner from "../../shared/LoadingSpinner/LoadingSpinner";

const backgroundStyle = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  width: '100vw',
  height: '93.4vh',
  backgroundImage: `url(${background})`,
  position: 'relative',
  overflow: 'hidden'
};

const Home = () => {

  return (
      <div style={backgroundStyle}>
        <div className={style.banner}>
          <p className={style.title}>4224 oferte imobiliare actualizate periodic </p>
          <p className={style.subtitle}>Găsim proprietatea potrivită pentru fiecare client și clientul potrivit pentru
            fiecare proprietate</p>
          <div className={style.left}>
            Termeni si
            conditii&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Confidentialitate&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Feedback
          </div>
          <img
              src={socials}
              width="90"
              height="30"
              className={style.right}
              alt={""}/>
        </div>
      </div>
  )
}

export default Home