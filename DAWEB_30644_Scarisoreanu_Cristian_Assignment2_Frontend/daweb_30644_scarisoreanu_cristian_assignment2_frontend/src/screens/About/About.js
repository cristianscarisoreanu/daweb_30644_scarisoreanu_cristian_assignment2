import DescriptionPage from "../../shared/DescriptionPage/DescriptionPage";
import YoutubeFrame from "../../shared/YoutubeFrame/YoutubeFrame";
import style from './style.module.css'

const About = () => (
    <>
    <DescriptionPage
        title="Despre Noi"
        alignLeft={true}
        descriptions={
          [
            `Noi oferim o gamă completă de servicii imobiliare integrate, înainte, în timpul și după finalizarea tranzacției. 
      Acoperă toate tipurile de solicitări și prezintă clienților o paletă largă de opțiuni, 
      atât pe segmentul rezidențial, cât și pe cel comercial.`,
            `Clienții nostrii au acces la un call center dedicat, agenți specializați pe departamente și tipuri de tranzacții, 
      asistență juridică și consiliere financiară gratuită, facilitare de servicii de design interior, servicii de 
      mutare și de curățenie, dar și reduceri de până la 100% la produse și servicii oferite de cei peste 700 de 
      parteneri ai nostrii din peste 44 de domenii de activitate.`,
            `Echipa noastra își conduce întreaga activitate de standardele profesionale înalte atât în relațiile cu 
      clienții, partenerii, dar și cu propriii angajați și agenți. Cei peste 160 de angajați și agenți sunt 
      supuși continuu unui proces de formare și dezvoltare profesională continuă prin programe complete de 
      training, coaching și dezvoltare profesională.`,
            <YoutubeFrame style={{padding: '10px'}} embedId={'Z4-WgqknyZc'}/>
          ]
        }
        height={'50%'}
    />
    </>
)


export default About