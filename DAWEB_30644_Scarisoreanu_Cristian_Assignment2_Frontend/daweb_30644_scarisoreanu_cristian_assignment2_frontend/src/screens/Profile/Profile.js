import {useEffect, useState} from "react"
import style from './style.module.css'
import {Button, Form} from "react-bootstrap"
import axios from "axios"
import Cookies from "js-cookie"
import {useNavigate} from "react-router-dom"
import background from "../../images/1899348.jpg";

const backgroundStyle = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  width: '100vw',
  height: '93.4vh',
  backgroundImage: `url(${background})`,
  position: 'relative',
  overflow: 'hidden'
};

const Profile = ({show}) => {
  const navigate = useNavigate()
  const [loading, setLoading] = useState(false)
  const [isEditing, setIsEditing] = useState(false)
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    propertyType: [],
  })

  const handleInputChange = (event) => {
    const {name, value, checked} = event.target

    let selectedOptions = formData[name] || []

    if (event.target.type === "checkbox") {
      if (checked) {
        selectedOptions.push(value)
      } else {
        selectedOptions = selectedOptions.filter((option) => option !== value)
      }
    } else {
      selectedOptions = [value]
    }

    setFormData({...formData, [name]: selectedOptions})
  }


  const handleEditClick = () => {
    setIsEditing(true)
  }

  const handleSaveClick = () => {
    setIsEditing(false)

    axios.put(`${process.env.REACT_APP_BACKEND_URL}/profile`, {
      first_name: formData.firstName.toString(),
      last_name: formData.lastName.toString(),
      email: formData.email.toString(),
      interested_in_apartments: formData.propertyType.includes('apartments'),
      interested_in_houses: formData.propertyType.includes('houses'),
      interested_in_spaces: formData.propertyType.includes('spaces'),
    }, {
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken'),
        'Cookie': `sessionid=${Cookies.get('sessionid')}`,
      },
      withCredentials: true
    })
        .then(() => {
          setIsEditing(false)
        })
        .catch(error => {
          console.error(error)
        })
  }

  useEffect(() => {
    setLoading(true)
    axios.get(`${process.env.REACT_APP_BACKEND_URL}/profile`, {
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken'),
        'Cookie': `sessionid=${Cookies.get('sessionid')}`,
      },
      withCredentials: true
    })
        .then((response) => {
          setFormData({
            firstName: response.data.firstName,
            lastName: response.data.lastName,
            email: response.data.email,
            propertyType: [
              response.data.propertyType[0] ? 'apartments' : '',
              response.data.propertyType[1] ? 'houses' : '',
              response.data.propertyType[2] ? 'spaces' : '',
            ],
          })
        })
        .catch((error) => {
          console.error(error)
        })
        .finally(() => {
          setLoading(false)
        })
  }, [])

  useEffect(() => {
    if (!show) {
      navigate('/not_found')
    }
  })

  if ((!formData.firstName && !formData.lastName && !formData.email && !show) || loading) {
    return null
  }

  return (
      <div style={backgroundStyle}>
        <div className={style.container}>
          <h2>Profil</h2>
          <Form className={style.form}>
            <label className={style.label}>
              Nume:
              <input
                  type="text"
                  name="lastName"
                  value={formData.lastName}
                  onChange={handleInputChange}
                  disabled={!isEditing}
              />
            </label>

            <label className={style.label}>
              Prenume:
              <input
                  type="text"
                  name="firstName"
                  value={formData.firstName}
                  onChange={handleInputChange}
                  disabled={!isEditing}
              />
            </label>
            <label className={style.label}>
              Email:
              <input
                  type="email"
                  name="email"
                  value={formData.email}
                  onChange={handleInputChange}
                  disabled={!isEditing}
              />
            </label>
            <label className={style.label}>
              Tipuri de propietati:
              <div className={style.types}>
                <label className={style.label}>
                  <input
                      type="checkbox"
                      name="propertyType"
                      value="apartments"
                      checked={formData.propertyType.includes("apartments")}
                      onChange={handleInputChange}
                      disabled={!isEditing}
                  />
                  Apartamente
                </label>
                <label className={style.label}>
                  <input
                      type="checkbox"
                      name="propertyType"
                      value="houses"
                      checked={formData.propertyType.includes("houses")}
                      onChange={handleInputChange}
                      disabled={!isEditing}
                  />
                  Case
                </label>
                <label className={style.label}>
                  <input
                      type="checkbox"
                      name="propertyType"
                      value="spaces"
                      checked={formData.propertyType.includes("spaces")}
                      onChange={handleInputChange}
                      disabled={!isEditing}
                  />
                  Birouri
                </label>
              </div>
            </label>
          </Form>
          {isEditing ? (
              <Button className={style.btn} onClick={handleSaveClick}>Save</Button>
          ) : (
              <Button className={style.btn} onClick={handleEditClick}>Edit</Button>
          )}
        </div>
      </div>
  )
}

export default Profile
