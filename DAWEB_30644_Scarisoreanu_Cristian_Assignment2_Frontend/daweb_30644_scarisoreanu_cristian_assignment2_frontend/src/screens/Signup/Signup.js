import background from "../../images/thumb-1920-437716.jpg"
import AppNavbar from "../../shared/AppNavbar/AppNavbar"
import {useEffect, useState} from "react"
import style from './style.css'
import {Form, Input} from "reactstrap"
import axios from "axios"
import CSRFToken from "../../shared/CSRFToken/CSRFToken"
import Cookies from 'js-cookie'
import {
  NotificationsContainer,
  showErrorNotification,
  showNotification
} from "../../shared/ToastNotification/ToastNotification"
import {useNavigate} from "react-router-dom";
import LoadingSpinner from "../../shared/LoadingSpinner/LoadingSpinner";

const backgroundStyle = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  width: '100vw',
  height: '93.4vh',
  backgroundImage: `url(${background})`,
  position: 'absolute'
}

const Signup = ({authenticated}) => {
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [loading, setLoading] = useState(false)
  const navigate = useNavigate()

  const handleSubmit = (e) => {
    e.preventDefault()

    setLoading(true)
    axios.post(`${process.env.REACT_APP_BACKEND_URL}/signup`, {
      username: username,
      email: email,
      password: password,
      confirmPassword: confirmPassword,
    }, {
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken')
      },
      withCredentials: true
    })
        .then((response) => {
          showNotification(response.data.success)

          window.setTimeout(function () {
            navigate('/login')

          }, 2000);
        })
        .catch((error) => {
          if(error.response.status === 403) {
            showErrorNotification('Request failed: 403')
          } else {
            showErrorNotification(error.response.data.error)
          }
        })
  }

  useEffect(() => {
    if(authenticated) {
      navigate('/')
    }
  })

  if(authenticated) {
    return null
  }

  return (
      <div style={backgroundStyle}>
        <LoadingSpinner show={loading}/>
        <div className="signupPage">
          <NotificationsContainer/>
          <Form style={{marginTop: '15px'}} className="login" onSubmit={handleSubmit}>
            <CSRFToken/>
            <h2 style={{marginBottom: '15px', color: 'white'}}>Signup</h2>
            <Input className='input'
                   onChange={(e) => setUsername(e.target.value)}
                   value={username || ''} type="text"
                   placeholder="Username"
                   id="username"
                   name="username"
            />
            <Input className='input'
                   onChange={(e) => setEmail(e.target.value)}
                   value={email || ''} type="text"
                   placeholder="Email Address"
                   id="email"
                   name="email"
            />
            <Input className='input'
                   onChange={(e) => setPassword(e.target.value)}
                   value={password || ''}
                   type="password"
                   placeholder="Password"
                   id="password"
                   name="password"
            />
            <Input className='input'
                   onChange={(e) => setConfirmPassword(e.target.value)}
                   value={confirmPassword || ''}
                   type="password"
                   placeholder="Confirm password"
                   id="confirmPasswd"
                   name="confirmPasswd"
            />
            <button
                style={{
                  color: 'white',
                  marginTop: '10px',
                  width: '200px',
                  border: '1px solid white',
                  borderRadius: '13px',
                  backgroundColor: '#2B7890',
                  marginBottom: '20px'

                }} type="submit">Sign up
            </button>
          </Form>
        </div>
      </div>
  )
}

export default Signup