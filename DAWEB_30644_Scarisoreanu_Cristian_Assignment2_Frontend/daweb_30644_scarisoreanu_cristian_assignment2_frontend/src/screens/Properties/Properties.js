import Container from 'react-bootstrap/Container'
import {Button} from 'react-bootstrap'
import {useEffect, useState} from 'react'
import style from './style.module.css'
import PropertiesTable from "./components/PropertiesTable";
import PropertiesModal from "./components/PropertiesModal";
import axios from "axios";
import Cookies from "js-cookie";
import {useNavigate} from "react-router-dom";

const Properties = ({show}) => {
  const [properties, setProperties] = useState([])
  const [showModal, setShowModal] = useState(false)
  const navigate = useNavigate()

  useEffect(() => {
    if (!show) {
      navigate('/not_found')
    }
  })

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_BACKEND_URL}/properties`, {
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken'),
        'Cookie': `sessionid=${Cookies.get('sessionid')}`,
      },
      withCredentials: true
    })
        .then((response) => {
          setProperties(response.data)
        })
        .catch((error) => {
          console.error(error)
        })
  }, [])

  if (!show) {
    return null
  }

  return (
      <>
        <Container fluid className={style.container}>
          <h3>Management</h3>
          <div className={style.addButton}>
            <Button color="success" onClick={() => setShowModal(true)}>Adauga proprietate</Button>
            <PropertiesModal properties={properties} setProperties={setProperties} showModal={showModal}
                             setShowModal={setShowModal} property={null}/>
          </div>
          <PropertiesTable properties={properties} setProperties={setProperties}/>
        </Container>
      </>
  )
}

export default Properties
