import React, {useEffect, useState} from 'react';
import style from '../style.module.css'
import {Button} from 'react-bootstrap';
import axios from "axios";
import Cookies from "js-cookie";
import LoadingSpinner from "../../../shared/LoadingSpinner/LoadingSpinner";

const PropertiesModal = ({showModal, setShowModal, property, properties, setProperties}) => {
  const [loading, setLoading] = useState(false)
  const [formattedImage, setFormattedImage] = useState(null)
  const [formData, setFormData] = useState({
    description: property ? property.description : '',
    price: property ? property.price : '',
    property_type: property ? property.property_type : 'Apartament',
    listing_type: property ? property.listing_type : 'De inchiriat',
    image: property ? property.image : null
  })

  const resetForm = () => {
    setFormData({
      description: property ? property.description : '',
      price: property ? property.price : '',
      property_type: property ? property.property_type : 'Apartament',
      listing_type: property ? property.listing_type : 'De inchiriat',
      image: property ? property.image : null
    });
  };

  const handleChange = (e) => {
    if (e.target.name === "image") {
      setFormattedImage(
          e.target.files[0],
      )
    } else {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value,
      });
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);

    let data = formData;
    data['image'] = formattedImage


    if (!property) {
      axios.post(
          `${process.env.REACT_APP_BACKEND_URL}/properties`,
          data,
          {
            headers: {
              'X-CSRFToken': Cookies.get('csrftoken'),
              'Cookie': `sessionid=${Cookies.get('sessionid')}`,
              'Content-Type': 'multipart/form-data'
            },
            withCredentials: true,
          }
      ).then((response) => {
        setProperties([...properties, response.data]);
      }).catch((error) => {
        console.error('Error adding item:', error);
      }).finally(() => {
        setLoading(false);
      });
    } else {
      axios.put(
          `${process.env.REACT_APP_BACKEND_URL}/properties/${property.id}/`,
          data,
          {
            headers: {
              'X-CSRFToken': Cookies.get('csrftoken'),
              'Cookie': `sessionid=${Cookies.get('sessionid')}`,
              'Content-Type': 'multipart/form-data'
            },
            withCredentials: true,
          }
      ).then((response) => {
        console.log(formData)
        const updatedProperties = properties.map(searchedProperty => {
          if (searchedProperty.id === property.id) {
            return formData;
          }
          return searchedProperty;
        })
        setProperties(updatedProperties)
      }).catch((error) => {
        console.error('Error updating item:', error);
      }).finally(() => {
        setLoading(false);
      });
    }

    setShowModal(false);
  }

  const handleClickOutside = (e) => {
    if (e.target.closest(`.${style.modalContent}`) === null) {
      setShowModal(false)
    }
  }

  const handleKeyDown = (e) => {
    if (e.keyCode === 27) {
      setShowModal(false)
    }
  };

  useEffect(() => {
    if (showModal) {
      document.addEventListener('mousedown', handleClickOutside)
      document.addEventListener('keydown', handleKeyDown)
      resetForm()
    } else {
      document.removeEventListener('mousedown', handleClickOutside)
      document.removeEventListener('keydown', handleKeyDown)
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
      document.removeEventListener('keydown', handleKeyDown)
    };
  }, [showModal])


  return (
      <>
        {showModal ? (
            <>
              <LoadingSpinner show={loading}/>
              <div className={style.modal}
                   style={property ? {backgroundColor: 'rgba(0, 0, 0, 0.04)'} : {backgroundColor: 'rgba(0, 0, 0, 0.4)'}}>
                <div className={style.modalContent}>
            <span className={style.close} onClick={() => setShowModal(false)}>
              &times;
            </span>
                  <div className={style.formContainer}>
                    <form onSubmit={handleSubmit}>
                      <h1>{property ? 'Editeaza proprietate' : 'Adauga proprietate'}</h1>

                      <div className={style.inputContainer}>
                        <label htmlFor="description">Descriere</label>
                        <input type="text" id="description" name="description" value={formData.description}
                               onChange={handleChange}/>
                      </div>

                      <div className={style.inputContainer}>
                        <label htmlFor="property_type">Tip de proprietate</label>
                        <select id="property_type" name="property_type" value={formData.property_type}
                                onChange={handleChange}>
                          <option value="Apartament">Apartament</option>
                          <option value="Casa">Casa</option>
                          <option value="Birou">Birou</option>
                        </select>
                      </div>

                      <div className={style.inputContainer}>
                        <label htmlFor="listingType">Tip de listare</label>
                        <select id="listing_type" name="listing_type" value={formData.listing_type}
                                onChange={handleChange}>
                          <option value="De inchiriat">De inchiriat</option>
                          <option value="De vanzare">De vanzare</option>
                        </select>
                      </div>

                      <div className={style.inputContainer}>
                        <label htmlFor="price">Pret</label>
                        <input type="number" id="price" name="price" value={formData.price} onChange={handleChange}/>
                      </div>

                      <div className={style.inputContainer}>
                        <label htmlFor="image">Imagine</label>
                        <input type="file" id="image" name="image" onChange={handleChange}/>
                      </div>

                      <Button type="submit">Submit</Button>
                    </form>
                  </div>
                </div>
              </div>
            </>
        ) : null}
      </>
  );
};

export default PropertiesModal
