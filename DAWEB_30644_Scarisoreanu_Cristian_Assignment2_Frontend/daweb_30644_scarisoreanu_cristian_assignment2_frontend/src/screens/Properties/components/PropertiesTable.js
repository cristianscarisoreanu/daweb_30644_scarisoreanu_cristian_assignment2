import {Button, ButtonGroup, Table} from 'react-bootstrap'
import PropertiesModal from './PropertiesModal'
import {useState} from 'react'
import axios from "axios";
import Cookies from "js-cookie";
import LoadingSpinner from "../../../shared/LoadingSpinner/LoadingSpinner";

const PropertiesTable = ({properties, setProperties}) => {
  const [showModal, setShowModal] = useState(false)
  const [selectedProperty, setSelectedProperty] = useState(null)
  const [loading, setLoading] = useState(false)

  const deleteProperty = (location) => {
    setLoading(true)
    axios.delete(
        `${process.env.REACT_APP_BACKEND_URL}/properties/${location.id}`,
        {
          headers: {
            'X-CSRFToken': Cookies.get('csrftoken')
          },
          withCredentials: true
        }
    )
        .then(() => {
          let updatedProperties = [...properties].filter(i => i.id !== location.id);
          setProperties(updatedProperties);
        })
        .catch(error => {
          console.error('Error deleting item:', error);
          // handle error
        })
        .finally(() => {
          setLoading(false)
        })
  }

  const propertyList = properties.map(property => {
    return <tr key={property.id}>
      <td> {property.description}</td>
      <td> {property.property_type} </td>
      <td> {property.listing_type} </td>
      <td> {property.price} </td>
      <td>
        <ButtonGroup>
          <Button className='btn btn-warning button mx-lg-2'
                  onClick={() => {
                    setShowModal(true);
                    setSelectedProperty(property)
                  }}>Editeaza
          </Button>
          <PropertiesModal showModal={showModal} setShowModal={setShowModal} property={selectedProperty}
                           properties={properties} setProperties={setProperties}/>
          <Button onClick={() => deleteProperty(property)} type="button"
                  className='btn btn-danger button mx-lg-2'>Sterge</Button>
        </ButtonGroup>
      </td>
    </tr>
  })

  return (
      <>
        <LoadingSpinner show={loading}/>
        <Table className="mt-4">
          <thead>
          <tr>
            <th width='20%'>Descriere</th>
            <th width='20%'>Tip de proprietate</th>
            <th width='20%'>Tip de listare</th>
            <th width='20%'>Pret</th>
            <th width='10%'>Actiuni</th>
          </tr>
          </thead>
          <tbody>
          {propertyList}
          </tbody>
        </Table>
      </>
  )
}

export default PropertiesTable
