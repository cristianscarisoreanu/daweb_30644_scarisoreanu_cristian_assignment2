import background from "../../images/thumb-1920-437716.jpg";
import {useEffect, useState} from "react";
import {Form, Input} from "reactstrap";
import CSRFToken from "../../shared/CSRFToken/CSRFToken";
import axios from "axios";
import Cookies from "js-cookie";
import style from './style.module.css'
import {
  NotificationsContainer,
  showErrorNotification,
  showNotification
} from "../../shared/ToastNotification/ToastNotification";
import {useNavigate} from "react-router-dom";
import LoadingSpinner from "../../shared/LoadingSpinner/LoadingSpinner";

const backgroundStyle = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  width: '100vw',
  height: '93.4vh',
  backgroundImage: `url(${background})`,
  position: 'absolute'
};

const Login = ({authenticated, setAuthenticated, setClient, setAdmin}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate()

  const handleSubmit = (e) => {
    e.preventDefault()
    setLoading(true)
    axios.post(`${process.env.REACT_APP_BACKEND_URL}/login`, {
      username: username,
      password: password,
    }, {
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken')
      },
      withCredentials: true
    })
        .then((response) => {
          showNotification(response.data.success)

          window.setTimeout(function () {
            setAuthenticated(true)
            setClient(response.data.role === 'client')
            setAdmin(response.data.role === 'admin')
            navigate('/')

          }, 2000);
        })
        .catch((error) => {
          if (error.response.status === 403) {
            showErrorNotification('Request failed: 403')
          } else {
            showErrorNotification(error.response.data.error)
            setLoading(false)
          }
        })
  }

  useEffect(() => {
    if(authenticated) {
      navigate('/')
    }
  }, [authenticated, navigate])

  if(authenticated) {
    return null
  }

  return (
      <div style={backgroundStyle}>
        <LoadingSpinner show={loading}/>
        <div className={style.loginPage}>
          <NotificationsContainer/>
          <Form name="login" style={{marginTop: '30px'}} className={style.login} onSubmit={handleSubmit}>
            <CSRFToken/>
            <h2 style={{marginBottom: '40px', color: 'white'}}>Login</h2>
            <Input className='input'
                   onChange={(e) => setUsername(e.target.value)}
                   value={username || ''} type="text"
                   placeholder="Username"
                   id="username"
                   name="username"
            />
            <Input className='input'
                   onChange={(e) => setPassword(e.target.value)}
                   value={password || ''}
                   type="password"
                   placeholder="*************"
                   id="password"
                   name="password"
            />
            <button
                style={{
                  color: 'white',
                  marginTop: '10px',
                  width: '200px',
                  border: '1px solid white',
                  borderRadius: '13px',
                  backgroundColor: '#2B7890',
                  marginBottom: '20px'

                }} type="submit">Log in
            </button>
            <p className='rememberMe'
               style={{textDecoration: 'underline', float: 'right', textAlign: 'right', fontSize: '15px'}}>Forgot
              Password?</p>
          </Form>
        </div>
      </div>
  )
}

export default Login