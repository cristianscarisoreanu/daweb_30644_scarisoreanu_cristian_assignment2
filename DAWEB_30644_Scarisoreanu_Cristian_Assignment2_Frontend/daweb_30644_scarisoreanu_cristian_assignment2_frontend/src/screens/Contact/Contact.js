import DescriptionPage from "../../shared/DescriptionPage/DescriptionPage";
import {Form, Input} from "reactstrap";
import {useState} from "react";

const Contact = () => {
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [msg, setMsg] = useState('');

  const validateForm = () => {
    if (name === "") {
      alert("Introduceti numele!");
      return false
    } else if (phone === "") {
      alert("Introduceti numarul de telefon!")
      return false;
    } else if (email === "") {
      alert("Introduceti email-ul!");
      return false;
    } else if(!(/^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/.test(email))) {
      alert("Adresa de email nu este corecta!");
      return false
    } else if (msg.length < 15) {
      alert("Mesajul este prea scurt")
      return false
    } else if (phone.length < 11) {
      alert('Numarul de telefon nu este valid!')
    }

    return true
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const valid = validateForm()

    if(valid) {
      alert('Trimis cu success!')
    }
    // TO DO - BACKEND
    // setLoading(true);
    // e.preventDefault()
    // const params = {
    //   body: {
    //     username: username,
    //     password: password
    //   }
    // }
    // API_LOGIN.login(params, (result, status, err) => {
    //   if (result !== null && status === 200) {
    //     setSuccess(true)
    //     localStorage.setItem('token', result.token)
    //     setUsername('')
    //     setPassword('')
    //
    //     showNotification('Logged in successfully!')
    //
    //     setTimeout(function () {
    //       if (isAdmin())
    //         navigate('/clients')
    //       else
    //         navigate('/devices')
    //     }, 2000);
    //
    //   } else {
    //     setError(err);
    //     setLoading(false)
    //     showErrorNotification('Bad credentials!')
    //  }
    // })
  }

  return (
      <DescriptionPage
          title="Contact"
          descriptions={
            [
              <h3 style={{fontStyle: 'normal'}}>Adresa</h3>,
              <p style={{color: 'gray'}}>Strada Mehedinti 65-67, Cluj-Napoca, Cluj</p>,
              <br/>,
              <h3 style={{fontStyle: 'normal'}}>Contacteaza</h3>,
              <p style={{color: 'gray'}}>Telefon: 0752109824</p>,
              <p style={{color: 'gray'}}>Email: office@scari.ro</p>,
              <br/>,
              <h3 style={{fontStyle: 'normal'}}>Orar</h3>,
              <p style={{color: 'gray'}}>Luni - Vineri: 8:00 - 19:00</p>,
              <p style={{color: 'gray'}}>Sambata: 9:00 - 14:00</p>,
              <br/>,

              <h3 style={{fontStyle: 'normal'}}>Social media:</h3>,
              <div style={{marginLeft: '3px'}}>
                <a style={{color: 'grey', marginRight: '25px', fontSize: '1.5vw', textDecoration: 'none'}}
                   href="https://www.facebook.com/cristianeusebiu.scarisoreanu/"
                   className="fa fa-facebook"/>
                <a style={{color: 'grey', marginRight: '25px', fontSize: '1.5vw', textDecoration: 'none'}}
                   href="https://www.facebook.com/cristianeusebiu.scarisoreanu/"
                   className="fa fa-twitter"/>
                <a style={{color: 'grey', marginRight: '25px', fontSize: '1.5vw', textDecoration: 'none'}}
                   href="https://www.facebook.com/cristianeusebiu.scarisoreanu/"
                   className="fa fa-instagram"/>
              </div>,
            ]
          }
          html={
            []
          }
          alignLeft={true}
          noColumns={2}
          forms={[
            <Form style={{marginTop: '25px'}} className="login" onSubmit={handleSubmit}>
              <h2 style={{marginBottom: '50px', color: 'black'}}>Trimite-ne un mesaj:</h2>
              <Input className='inputEmail'
                     onChange={(e) => setName(e.target.value)}
                     value={name || ''} type="text"
                     placeholder="Nume si prenume"
                     id="username"
                     name="username"
              />
              <Input className=''
                     onChange={(e) => setPhone(e.target.value)}
                     value={phone || ''}
                     placeholder="Telefon"
                     id="telefon"
                     name="username"
              />
              <Input className='inputTelefon'
                     onChange={(e) => setEmail(e.target.value)}
                     value={email || ''}
                     placeholder="Email"
                     id="username"
                     name="username"
              />
              <Input className='inputMesaj'
                     onChange={(e) => setMsg(e.target.value)}
                     value={msg || ''}
                     placeholder="Mesaj"
                     id="username"
                     name="username"
              />

              <button
                  style={{
                    color: 'white',
                    marginTop: '10px',
                    width: '200px',
                    border: '1px solid white',
                    borderRadius: '13px',
                    backgroundColor: '#2B7890',
                    marginBottom: '20px'

                  }} type="submit">Trimite
              </button>
            </Form>
          ]}
      />
  )
}

export default Contact