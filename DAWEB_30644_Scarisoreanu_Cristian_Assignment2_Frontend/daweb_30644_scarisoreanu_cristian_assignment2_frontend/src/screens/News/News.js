import AppNavbar from "../../shared/AppNavbar/AppNavbar";
import style from "../About/style.module.css";
import image from "../../images/Newsroom_Hero-b1aded 2.png";
import news1 from "../../images/first news.png"
import news2 from "../../images/news2.jpg"
import news3 from "../../images/news3.jpg"
import news4 from "../../images/news4.jpg"
import CardGrid from "../../shared/CardGrid/CardGrid";

const articles = [
  [
    'Începe Târgul Imobiliar Timișoara TIT...',
    news1,
    `Iulius Congress Hall găzduiește în data de 18-19 martie ediția de primăvară 
    a Târgului Imobiliar Timișoara TIT. Evenimentul reunește, ca de obicei, expozanți din 
    domeniul imobiliar și sectorul bancar,...`,
    '16 mart. 2023',
    'citeste mai departe',
    true,
  ],
  [
    'Proiect de lege: Locuințele din clădirile...',
    news2,
    `Alertați de cutremurele manifestate în România, oficialii statului vin cu o serie de modificări 
    privind clădirile aflate în clasa I de risc seismic. La nivel național există 2.687 de...`,
    '8 mart. 2023',
    'citeste mai departe',
    true,
  ],
  [
    'În ciuda așteptărilor, prețurile...',
    news3,
    `Variații pozitive au avut loc pe ambele segmente de piață, însă cele mai semnificative s-au 
    înregistrat pe segmentul rezidențial nou. După un avans de 0,9% în ianuarie, prețurile locuințelor...`,
    '7 mart. 2023',
    'citeste mai departe',
    true,
  ],
  [
    'Piața imobiliară în 2023...',
    news4,
    `După o scădere de 1,3% înregistrată în luna decembrie, prețurile locuințelor au înregistrat 
    o evoluție pozitivă în prima lună din 2023...`,
    '2 feb 2023',
    'citeste mai departe',
    true,
  ]
]
const News = () => {
  return (
      <div>
        <div className={style.divImage}>
          <img className={style.splitImage} src={image} alt={""}/>
          <h1>Stiri si noutati</h1>
        </div>
        <div style={{padding: '75px'}}>
          <CardGrid applyPadding={false} cardDetails={articles} seeMore/>
        </div>
      </div>
  )
}

export default News