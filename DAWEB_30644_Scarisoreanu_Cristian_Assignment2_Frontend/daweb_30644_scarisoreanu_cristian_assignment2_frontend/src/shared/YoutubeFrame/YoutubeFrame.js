import React from "react";
import style from './style.module.css'
const YoutubeFrame = ({ embedId }) => (
    <div className={style.videoResponsive} style={{marginTop: '35px', marginLeft: '55px'}}>
      <iframe
          width="930"
          height="480"
          src={`https://www.youtube.com/embed/${embedId}`}
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
          title="Embedded youtube"
      />
    </div>
);

export default YoutubeFrame;