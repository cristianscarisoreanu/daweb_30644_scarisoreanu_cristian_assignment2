import React from 'react'
import { ToastContainer, Slide, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

export const showNotification = (message, duration = 5000) => {
  toast.success(message, {
    autoClose: duration,
  })
}

export const showInfo = (message, duration = 5000) => {
  toast.info(message, {
    autoClose: duration,
  })
}

export const showErrorNotification = (message, duration = 5000) => {
  toast.error(message, {
    autoClose: duration,
  })
}

export const NotificationsContainer = () => (
    <ToastContainer
        position="top-right"
        transition={Slide}
        hideProgressBar
        closeOnClick
        pauseOnFocusLoss
        draggable={false}
        pauseOnHover={false}
    />
)