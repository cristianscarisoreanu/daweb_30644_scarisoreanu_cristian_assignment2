import React, {useEffect, useState} from 'react'
import axios from 'axios'

const CSRFToken = () => {
  const [csrftoken, setcsrftoken] = useState('')

  const getCookie = (name) => {
    let cookieValue = null
    if (document.cookie && document.cookie !== '') {
      let cookies = document.cookie.split(';')
      for (const element of cookies) {
        let cookie = element.trim()
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
          break
        }
      }
    }
    return cookieValue
  }

  useEffect(() => {
    const fetchData = async () => {
      try {
        await axios.get(`${process.env.REACT_APP_BACKEND_URL}/csrf_token`, {withCredentials: true})
      } catch (err) {
      }
    }

    fetchData()
    setcsrftoken(getCookie('csrftoken'))
  }, [])

  return (
      <input type='hidden' name='csrfmiddlewaretoken' value={csrftoken}/>
  )
}

export default CSRFToken