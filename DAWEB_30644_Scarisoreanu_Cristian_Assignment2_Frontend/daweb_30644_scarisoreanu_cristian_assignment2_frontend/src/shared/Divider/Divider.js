import style from './style.module.css'

const Divider = ({children}) => {
  return (
      <div className={style.container}>
        <div className={style.border}/>
        {children && (
            <span className={style.content}>
        {children}
      </span>
        )}
        <div className={style.border}/>
      </div>
  );
};


export default Divider