import React from 'react'
import background from '../../images/background.jfif'
import AppNavbar from "../AppNavbar/AppNavbar";

const backgroundStyle = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  width: '100vw',
  height: '100vh',
  backgroundImage: `url(${background})`,
  position: 'absolute'
};

const ErrorPage = () => {
  return (
      <div>
        <div style={backgroundStyle}>
          <h2 style={{color: 'white'}}>404 - Pagina nu a fost gasita</h2>
        </div>

      </div>
  );
}

export default ErrorPage