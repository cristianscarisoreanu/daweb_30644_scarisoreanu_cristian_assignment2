import AppNavbar from "../AppNavbar/AppNavbar";
import style from "../../screens/About/style.module.css";
import image from "../../images/Newsroom_Hero-b1aded 2.png";
import {Col, Row} from "react-bootstrap";

const DescriptionPage = ({title, descriptions, forms, alignLeft, noColumns, height}) => {
  const alignment = alignLeft ? {textAlign: 'left'} : {textAlign: 'center'}
  const maxHeight = height ? {height: height} : {height: ''}

  return (
      <div>
        <div className={style.divImage}>
          <img className={style.splitImage} src={image} alt={""}/>
          <h1>{title}</h1>
        </div>
        <div className={style.descriptionBox} style={maxHeight}>
          <Row>
            <Col md={noColumns === 2 ? 6 : 12}>
              <div style={alignment} className={style.description}>
                {descriptions.map((description) => {
                  if (description instanceof Object)
                    return description

                  return (
                      <>
                        <p>{description}</p>
                        <br/>
                      </>
                  )
                })}
              </div>
              </Col>
            {noColumns === 2 && (
                <Col md={noColumns === 2 ? 6 : 0}>
                  <div style={alignment} className={style.description}>
                    {forms && forms.map((form) => {
                      if (form instanceof Object)
                        return form

                      return (
                          <>
                            <p>{form}</p>
                            <br/>
                          </>
                      )
                    })}
                  </div>
                </Col>
            )}
          </Row>
        </div>
      </div>
)
}

export default DescriptionPage