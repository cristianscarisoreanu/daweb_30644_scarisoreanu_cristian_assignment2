import {Grid} from "@mui/material";
import CardItem from "../CardItem/CardItem";
import Divider from "../Divider/Divider";
import style from "../CardItem/style.module.css";

const CardGrid = ({title, cardDetails, divider, applyPadding, seeMore}) => {
  const padding = applyPadding ? {padding: '50px'} : null
  return (
      <>
        {title && (
            <h2>{title}</h2>
        )}
        <div className={style.mainDiv} style={padding}>
          <Grid container spacing={8}>
            {cardDetails.map((details) => {
                  return (
                      <Grid item xs={3}>
                        <CardItem
                            title={details[0]}
                            image={details[1]}
                            description={details[2]}
                            overlayText={details[3]}
                            buttonText={details[4]}
                            height={details[5]}
                            alignLeft={details[6]}
                        />
                      </Grid>
                  )
                }
            )}
          </Grid>
          {seeMore === undefined && (
              <div className={style.more}>
                <p>Vezi mai mult</p>
              </div>
          )}
        </div>
        {divider && (
            <Divider/>
        )}
      </>
  )
}

export default CardGrid