import React from "react";
import style from './style.module.css'

const LoadingSpinner = ({ show }) => {
  return (
      show && (
          <div className={style.loadingSpinnerOverlay}>
            <div className={style.loadingSpinner}/>
          </div>
      )
  )
}

export default LoadingSpinner;
