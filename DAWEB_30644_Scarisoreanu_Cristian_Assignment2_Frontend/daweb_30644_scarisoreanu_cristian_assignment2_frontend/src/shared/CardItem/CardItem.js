import Card from 'react-bootstrap/Card';
import style from './style.module.css'

const CardItem = ({image, description, title, overlayText, height, alignLeft, buttonText}) => {
  const cardStyling = height ? {height: height } : null
  const textAlign = alignLeft ? {textAlign: 'left'} : {textAlign: 'center'}

  return (
      <Card style={cardStyling} className={style.card}>
        <Card.Img className={style.cardImg} variant="top" src={image}/>
        <div className={style.overlay}>
          <p>{overlayText}</p>
        </div>
        <Card.Body>
          <Card.Title style={{color: 'blue'}}>{title}</Card.Title>
          <Card.Text style={{marginTop: '25px'}}>
            {description}
          </Card.Text>
        </Card.Body>
        <a style={textAlign} href='/' className={style.readMore}>{buttonText}</a>
      </Card>
  );
}

export default CardItem;