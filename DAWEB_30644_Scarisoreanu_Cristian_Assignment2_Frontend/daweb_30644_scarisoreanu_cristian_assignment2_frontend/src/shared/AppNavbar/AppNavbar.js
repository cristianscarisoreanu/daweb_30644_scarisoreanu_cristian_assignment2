import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import logo from '../../images/logo.png'
import {NavDropdown} from "react-bootstrap";
import {useState} from "react";
import axios from "axios";
import Cookies from "js-cookie";
import {useNavigate} from "react-router-dom";

const AppNavbar = ({authenticated, setAuthenticated, isClient, isAdmin}) => {
  const [showRents, setShowRents] = useState(false)
  const [showSales, setShowSales] = useState(false)
  const navigate = useNavigate()

  const logout = async () => {
    try {
      const response = await axios.post(
          `${process.env.REACT_APP_BACKEND_URL}/logout`,
          {},
          {
            headers: {
              'X-CSRFToken': Cookies.get('csrftoken'),
            },
            withCredentials: true,
          }
      )
      if (response.status === 200) {
        setAuthenticated(false)
        navigate('/login')
      }
    } catch (error) {
      console.error(error)
    }
  }


  const showSalesDropdown = (e) => {
    setShowSales(!showSales);
  }
  const hideSalesDropdown = e => {
    setShowSales(false);
  }

  const showRentsDropdown = (e) => {
    setShowRents(!showRents);
  }
  const hideRentsDropdown = e => {
    setShowRents(false);
  }

  return (
      <Navbar collapseOnSelect expand="xxl" bg="dark" variant="dark">
        <Container style={{maxWidth: '1600px'}}>
          <Navbar.Brand href="/">
            <img
                src={logo}
                width="35"
                height="35"
                className="d-inline-block align-top logo"
                alt={""}/>{' '}
            <text style={{fontFamily: 'Itim, cursive'}}>Scari's Homes</text>
          </Navbar.Brand>

          <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link style={{color: 'white'}} href="/">Acasa</Nav.Link>
              <Nav.Link style={{color: 'white'}} href="/news">Noutati</Nav.Link>
              <Nav.Link style={{color: 'white'}} href="/about">Despre noi</Nav.Link>
              <NavDropdown title="Inchirieri"
                           id="collasible-nav-dropdown"
                           show={showRents}
                           style={{color: 'white'}}
                           onMouseEnter={showRentsDropdown}
                           onMouseLeave={hideRentsDropdown}
              >
                <NavDropdown.Item href="/rentals/apartments">Apartamente de inchiriat</NavDropdown.Item>
                <NavDropdown.Divider/>
                <NavDropdown.Item href="/rentals/houses">Case de inchiriat</NavDropdown.Item>
                <NavDropdown.Divider/>
                <NavDropdown.Item href="/rentals/spaces">Spatii de inchiriat</NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="Vanzari"
                           id="collasible-nav-dropdown"
                           show={showSales}
                           style={{color: 'white'}}
                           onMouseEnter={showSalesDropdown}
                           onMouseLeave={hideSalesDropdown}
              >
                <NavDropdown.Item href="/sales/apartments">Apartamente de vanzare</NavDropdown.Item>
                <NavDropdown.Divider/>
                <NavDropdown.Item href="/sales/houses">Case de vanzare</NavDropdown.Item>
                <NavDropdown.Divider/>
                <NavDropdown.Item href="/sales/spaces">Spatii de vanzare</NavDropdown.Item>
              </NavDropdown>
              <Nav.Link style={{color: 'white'}} href="/contact">Contact</Nav.Link>
            </Nav>
            <Nav>
              {authenticated ? (
                  <>
                    {isClient && (
                        <Nav.Link style={{color: 'white'}} href="/profile">Profil</Nav.Link>
                    )}
                    {isAdmin && (
                        <Nav.Link style={{color: 'white'}} href="/properties">Propietati</Nav.Link>
                    )}
                    <Nav.Link style={{color: 'white'}} onClick={() => logout()}>Deconectare</Nav.Link>
                  </>
              ) : (
                  <>
                    <Nav.Link style={{color: 'white'}} href="/login">Autentificare</Nav.Link>
                    <Nav.Link style={{color: 'white'}} href="/signup">Inregistrare</Nav.Link>
                  </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
  )
}

export default AppNavbar