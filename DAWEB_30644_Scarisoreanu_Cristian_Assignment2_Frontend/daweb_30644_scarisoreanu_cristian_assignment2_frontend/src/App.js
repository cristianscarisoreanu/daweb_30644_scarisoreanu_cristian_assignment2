import './App.css'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import Home from "./screens/Home/Home"
import 'bootstrap/dist/css/bootstrap.min.css'
import Login from "./screens/Login/Login"
import Signup from "./screens/Signup/Signup"
import About from "./screens/About/About"
import Contact from "./screens/Contact/Contact"
import News from "./screens/News/News"
import Sales from "./screens/Sales/Sales"
import Rentals from "./screens/Rentals/Rentals"
import ErrorPage from "./shared/ErrorPage/ErrorPage"
import {useEffect, useState} from "react"
import AppNavbar from "./shared/AppNavbar/AppNavbar"
import axios from "axios"
import Cookies from "js-cookie"
import Profile from "./screens/Profile/Profile"
import Properties from "./screens/Properties/Properties";

const App = () => {
  const [authenticated, setAuthenticated] = useState(false)
  const [client, setClient] = useState(false)
  const [admin, setAdmin] = useState(false)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      localStorage.setItem('latitude', position.coords.latitude.toString())
      localStorage.setItem('longitude', position.coords.longitude.toString())
    })
  })

  useEffect(() => {
    const getAuthenticated = async () => {
      try {
        const response = await axios.get(
            `${process.env.REACT_APP_BACKEND_URL}/authenticated`,
            {
              headers: {
                'X-CSRFToken': Cookies.get('csrftoken'),
              },
              withCredentials: true
            }
        )
        setAdmin(response.data.admin)
        setClient(response.data.client)
        setAuthenticated(response.data.is_authenticated)
      } catch (error) {
      } finally {
        setLoading(false);
      }
    }

    getAuthenticated()
  }, [])

  if (loading) {
    return null
  }

  return (
      <Router>
        <AppNavbar authenticated={authenticated} setAuthenticated={setAuthenticated} isClient={client} isAdmin={admin}/>
        <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route path='*' element={<ErrorPage/>}/>
          <Route exact path="/login"
                 element={<Login authenticated={authenticated} setAuthenticated={setAuthenticated} setClient={setClient}
                                 setAdmin={setAdmin}/>}/>
          <Route exact path="/signup" element={<Signup authenticated={authenticated}/>}/>
          <Route exact path="/about" element={<About/>}/>
          <Route exact path='/contact' element={<Contact/>}/>
          <Route exact path='/news' element={<News/>}/>
          <Route exact path='/sales/apartments' element={<Sales type={'apartments'}/>}/>
          <Route exact path='/sales/houses' element={<Sales type={'houses'}/>}/>
          <Route exact path='/sales/spaces' element={<Sales type={'spaces'}/>}/>
          <Route exact path='/rentals/apartments' element={<Rentals type={'apartments'}/>}/>
          <Route exact path='/rentals/houses' element={<Rentals type={'houses'}/>}/>
          <Route exact path='/rentals/spaces' element={<Rentals type={'spaces'}/>}/>
          <Route exact path='/profile' element={<Profile show={client}/>}/>
          <Route exact path='/properties' element={<Properties show={admin}/>}/>
        </Routes>
      </Router>
  )
}

export default App
