import imghdr
import os
import re
from os.path import join

from django.contrib import auth
from django.contrib.auth.models import User
from django.http import HttpResponseNotFound, FileResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.exceptions import ValidationError
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions

from daweb_project import settings
from daweb_project.settings import MEDIA_ROOT, MEDIA_URL
from .models import UserProfile, Property
from .serializers import UserProfileSerializer, PropertySerializer


class CheckAuthenticatedView(APIView):
    def get(self, request):
        try:
            is_authenticated = request.user.is_authenticated
            if is_authenticated:
                user_profile = UserProfile.objects.get(user=request.user)
                return Response({'is_authenticated': True, 'client': user_profile.role == 'client',
                                 'admin': user_profile.role == 'admin'},
                                status=status.HTTP_200_OK)
            else:
                return Response({'is_authenticated': False}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def is_valid_email(email):
    if not email:
        return False
    pattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'
    return bool(re.match(pattern, email))


class SignupView(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(csrf_protect)
    def post(self, request):
        try:
            data = request.data
            print(data)

            username = data.get('username')
            email = data.get('email')
            password = data.get('password')
            confirm_password = data.get('confirmPassword')

            if username == '' or email == '' or password == '' or confirm_password == '':
                return Response({'error': 'Please fill out all required fields'}, status=status.HTTP_400_BAD_REQUEST)

            if password != confirm_password:
                return Response({'error': 'Passwords do not match'}, status=status.HTTP_400_BAD_REQUEST)

            if User.objects.filter(username=username).exists():
                return Response({'error': 'Username already exists'}, status=status.HTTP_400_BAD_REQUEST)

            if not is_valid_email(email):
                return Response({'error': 'Invalid email format'}, status=status.HTTP_400_BAD_REQUEST)

            if len(password) < 6:
                return Response({'error': 'Password is too short'}, status=status.HTTP_400_BAD_REQUEST)

            user = User.objects.create_user(username=username, password=password)
            user_profile = UserProfile(user=user, email=email)
            user_profile.save()

            return Response({'success': 'User created successfully'}, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class LoginView(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(csrf_protect)
    def post(self, request):
        try:
            data = request.data

            username = data.get('username')
            password = data.get('password')

            if username == '' or password == '':
                return Response({'error': 'Please fill out all required fields'}, status=status.HTTP_400_BAD_REQUEST)

            user = auth.authenticate(username=username, password=password)

            if user is not None:
                user_profiles = UserProfile.objects.filter(user=user)

                if user_profiles.count() != 0:
                    auth.login(request, user)
                    return Response({'success': 'User authenticated', 'role': user_profiles.first().role},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({'error': 'Bad credentials'}, status.HTTP_400_BAD_REQUEST)
            else:
                return Response({'error': 'Bad credentials'}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class LogoutView(APIView):
    @method_decorator(csrf_protect)
    def post(self, request):
        try:
            auth.logout(request)
            return Response({'success': 'Logged out'}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CSRFTokenView(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(ensure_csrf_cookie)
    def get(self, request):
        try:
            return Response({'success': 'CSRF cookie set'}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UserProfileView(APIView):
    authentication_classes = [SessionAuthentication]
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        try:
            user = request.user
            user_profile = UserProfile.objects.get(user=user)

            return Response(
                {
                    'firstName': user_profile.first_name,
                    'lastName': user_profile.last_name,
                    'email': user_profile.email,
                    'propertyType': [
                        user_profile.interested_in_apartments,
                        user_profile.interested_in_houses, user_profile.interested_in_spaces
                    ]
                },
                status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request):
        try:
            user = request.user
            user_profile = UserProfile.objects.get(user=user)
            serializer = UserProfileSerializer(user_profile, data=request.data, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PropertyView(APIView):
    authentication_classes = [SessionAuthentication]
    permission_classes = (permissions.IsAuthenticated,)
    parser_classes = [MultiPartParser, FormParser]

    def get(self, request):
        properties = Property.objects.all()
        serializer = PropertySerializer(properties, many=True)
        return Response(serializer.data)

    def post(self, request):
        print(request.data)
        serializer = PropertySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        property = Property.objects.get(pk=pk)
        serializer = PropertySerializer(property, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        property = Property.objects.get(pk=pk)
        property.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PropertyViewRentalsApartments(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(csrf_protect)
    def get(self, request):
        print(MEDIA_ROOT, MEDIA_URL)
        properties = Property.objects.filter(listing_type='De inchiriat', property_type='Apartament')
        serializer = PropertySerializer(properties, many=True)
        return Response(serializer.data)


class PropertyViewRentalsHouses(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(csrf_protect)
    def get(self, request):
        properties = Property.objects.filter(listing_type='De inchiriat', property_type='Casa')
        serializer = PropertySerializer(properties, many=True)
        return Response(serializer.data)


class PropertyViewRentalsSpaces(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(csrf_protect)
    def get(self, request):
        properties = Property.objects.filter(listing_type='De inchiriat', property_type='Birou')
        serializer = PropertySerializer(properties, many=True)
        return Response(serializer.data)


class PropertyViewSalesApartments(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(csrf_protect)
    def get(self, request):
        print(MEDIA_ROOT, MEDIA_URL)
        properties = Property.objects.filter(listing_type='De vanzare', property_type='Apartament')
        serializer = PropertySerializer(properties, many=True)
        return Response(serializer.data)


class PropertyViewSalesHouses(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(csrf_protect)
    def get(self, request):
        properties = Property.objects.filter(listing_type='De vanzare', property_type='Casa')
        serializer = PropertySerializer(properties, many=True)
        return Response(serializer.data)



class PropertyViewSalesSpaces(APIView):
    permission_classes = (permissions.AllowAny,)

    @method_decorator(csrf_protect)
    def get(self, request):
        properties = Property.objects.filter(listing_type='De vanzare', property_type='Birou')
        serializer = PropertySerializer(properties, many=True)
        return Response(serializer.data)
