from django.contrib import admin

# Register your models here.
from RealEstateApp.models import UserProfile, Property

admin.site.register(UserProfile)
admin.site.register(Property)