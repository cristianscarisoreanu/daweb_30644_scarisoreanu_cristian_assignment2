from rest_framework import serializers

from RealEstateApp.models import UserProfile, Property
from daweb_project import settings


class UserProfileSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(allow_blank=True, required=False)
    first_name = serializers.CharField(allow_blank=True, required=False)
    last_name = serializers.CharField(allow_blank=True, required=False)

    class Meta:
        model = UserProfile
        fields = (
            'first_name', 'last_name', 'email', 'interested_in_apartments', 'interested_in_houses',
            'interested_in_spaces'
        )


class PropertySerializer(serializers.ModelSerializer):
    class Meta:
        model = Property
        fields = ['id', 'description', 'price', 'property_type', 'listing_type', 'image']

    def create(self, validated_data):
        image = self.context.get('request')
        if image:
            image = image.FILES.get('image')
        if image:
            validated_data['image'] = image
        return super().create(validated_data)

    def update(self, instance, validated_data):
        image = self.context.get('request')
        if image:
            image = image.FILES.get('image')
        if image:
            validated_data['image'] = image
        return super().update(instance, validated_data)
