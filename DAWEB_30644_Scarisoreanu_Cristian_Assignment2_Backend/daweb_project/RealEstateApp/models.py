from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=None)
    first_name = models.CharField(max_length=500, default='')
    last_name = models.CharField(max_length=500, default='')
    email = models.CharField(max_length=500, default='')
    phone = models.CharField(max_length=500, default='')
    city = models.CharField(max_length=500, default='')
    role = models.CharField(max_length=500, default='client')
    interested_in_apartments = models.BooleanField(default=False)
    interested_in_houses = models.BooleanField(default=False)
    interested_in_spaces = models.BooleanField(default=False)

class Property(models.Model):
    PROPERTY_TYPES = (
        ('Apartament', 'Apartament'),
        ('Casa', 'Casa'),
        ('Birou', 'Birou'),
    )

    LISTING_TYPES = (
        ('De inchiriat', 'De inchiriat'),
        ('De vanzare', 'De vanzare'),
    )

    description = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    property_type = models.CharField(max_length=50, choices=PROPERTY_TYPES)
    listing_type = models.CharField(max_length=50, choices=LISTING_TYPES)
    image = models.ImageField(null=True, blank=True)


