from django.conf.urls.static import static
from django.urls import path

from RealEstateApp.views import SignupView, CSRFTokenView, CheckAuthenticatedView, LoginView, LogoutView, \
    UserProfileView, PropertyView, PropertyViewRentalsApartments, PropertyViewRentalsHouses, PropertyViewRentalsSpaces, \
    PropertyViewSalesHouses, PropertyViewSalesApartments, PropertyViewSalesSpaces
from daweb_project import settings

urlpatterns = [
    path('signup', SignupView.as_view()),
    path('authenticated', CheckAuthenticatedView.as_view()),
    path('login', LoginView.as_view()),
    path('logout', LogoutView.as_view()),
    path('csrf_token', CSRFTokenView.as_view()),
    path('profile', UserProfileView.as_view()),
    path('properties', PropertyView.as_view(), name='property-list'),
    path('rentals/apartments', PropertyViewRentalsApartments.as_view()),
    path('rentals/houses', PropertyViewRentalsHouses.as_view()),
    path('rentals/spaces', PropertyViewRentalsSpaces.as_view()),
    path('sales/apartments', PropertyViewSalesApartments.as_view()),
    path('sales/houses', PropertyViewSalesHouses.as_view()),
    path('sales/spaces', PropertyViewSalesSpaces.as_view()),
    path('properties/<int:pk>/', PropertyView.as_view(), name='property-detail'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)