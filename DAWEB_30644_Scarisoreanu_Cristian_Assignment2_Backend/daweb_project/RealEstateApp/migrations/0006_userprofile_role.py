# Generated by Django 4.2 on 2023-04-30 19:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RealEstateApp', '0005_delete_client'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='role',
            field=models.CharField(default='', max_length=500),
        ),
    ]
