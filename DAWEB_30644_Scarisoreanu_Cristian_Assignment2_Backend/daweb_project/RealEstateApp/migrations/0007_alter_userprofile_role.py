# Generated by Django 4.2 on 2023-05-02 18:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RealEstateApp', '0006_userprofile_role'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='role',
            field=models.CharField(default='client', max_length=500),
        ),
    ]
